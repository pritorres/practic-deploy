import { Injectable } from '@nestjs/common';

@Injectable()
export class AppService {
  getHello(): string {
    const TEXT_WEB = process.env['TEXT_WEB'];
    const PORT_WEB = process.env['PORT_WEB'];
    console.log(TEXT_WEB);
    console.log(PORT_WEB);
    return `${TEXT_WEB}, y este esta en el puerto ${PORT_WEB}`;
  }
}
