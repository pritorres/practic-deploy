import { Injectable } from '@nestjs/common';

@Injectable()
export class AppService {
  getHello(): string {
    const TEXT = process.env['TEST_VAR'];
    const PORT = process.env['PORT'];

    return `${TEXT}, este proyecto esta en el puerto ${PORT}`;
  }
}
